package com.sherwinca.apiacfsw.aplicacion;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.sherwinca.apiacfsw.infraestructura.mapper.CiaMapper;
import com.sherwinca.apiacfsw.infraestructura.repository.CiaRepository;
import com.sherwinca.apiacfsw.modelo.dto.CiaDTO;

import org.jboss.logging.Logger;

@ApplicationScoped
public class CiaService {
    private static final Logger LOGGER = Logger.getLogger(CiaService.class);

    @Inject
    CiaRepository ciaRepository;

    @Inject
    CiaMapper ciaMapper;

    public List<CiaDTO> listarCia() throws Exception{
        try {
            return ciaRepository.listarCia()
                                .stream()
                                .map(ciaMapper::toCiaDto)
                                .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en CiaService - ["+ e.getMessage()+"]");
        }
    }
}
