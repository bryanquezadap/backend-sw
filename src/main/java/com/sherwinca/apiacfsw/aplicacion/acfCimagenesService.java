package com.sherwinca.apiacfsw.aplicacion;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.sherwinca.apiacfsw.infraestructura.mapper.AcfCimagenesMapper;
import com.sherwinca.apiacfsw.infraestructura.repository.AcfCimagenesRepository;
import com.sherwinca.apiacfsw.modelo.dto.AcfCimagenesDTO;

import org.jboss.logging.Logger;


@ApplicationScoped
public class AcfCimagenesService {
    private static final Logger LOGGER = Logger.getLogger(AcfCimagenesService.class);

    @Inject
    AcfCimagenesRepository AcfCimagenesRepository;

    @Inject
    AcfCimagenesMapper AcfCimagenesMapper;

    public List<AcfCimagenesDTO> listarAcfimagenes() throws Exception{
        try {
            return AcfCimagenesRepository.listarAcfimagenes()
                                         .stream()
                                         .map(AcfCimagenesMapper::toacfCimagenesDTO)
                                         .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en acfCimagenesService -[" + e.getMessage()+"]");
        }
    }

    public List<AcfCimagenesDTO> buscarImagenActivo(Long id_sistema) throws Exception{
        try {
            return AcfCimagenesRepository.listarImagenActivo(id_sistema)
                                         .stream()
                                         .map(AcfCimagenesMapper::toacfCimagenesDTO)
                                         .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en acfCimagenesService -[" + e.getMessage()+"]");
        }
    }

    @Transactional
    public AcfCimagenesDTO guardarImagenes(AcfCimagenesDTO AcfCimagenesDto) throws Exception{
        try {
            return AcfCimagenesRepository.guardarImagenes(AcfCimagenesDto);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeActivoService - [" + e.getMessage() + "]");
        }
        
    }
    

}
