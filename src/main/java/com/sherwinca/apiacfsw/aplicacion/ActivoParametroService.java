package com.sherwinca.apiacfsw.aplicacion;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.sherwinca.apiacfsw.infraestructura.mapper.ActivoParametroMapper;
import com.sherwinca.apiacfsw.infraestructura.repository.Vw_activo_parametroRepository;
import com.sherwinca.apiacfsw.modelo.acf.VwActivoParametro;
import com.sherwinca.apiacfsw.modelo.dto.ActivoParametroDTO;

import org.jboss.logging.Logger;


@ApplicationScoped
public class ActivoParametroService {
    private static final Logger LOGGER = Logger.getLogger(ActivoParametroService.class);

    @Inject
    Vw_activo_parametroRepository vw_activo_parametroRepository;

    @Inject
    ActivoParametroMapper activoParametroMapper;


    public List<ActivoParametroDTO> buscarActivoParametros() throws Exception{
        try {
            return vw_activo_parametroRepository.buscarActivosParametro()
                                                .stream()
                                                .map(activoParametroMapper::activoParametroDTO)
                                                .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en ActivoParametroService - ["+ e.getMessage()+"]");
        }
    }

    public ActivoParametroDTO buscarActivoParametro(Long id_sistema) throws Exception {
        VwActivoParametro mafParametro = vw_activo_parametroRepository.buscarActivoParametro(id_sistema);
        return mafParametro != null ? activoParametroMapper.activoParametroDTO(mafParametro) : null;
    }

}
