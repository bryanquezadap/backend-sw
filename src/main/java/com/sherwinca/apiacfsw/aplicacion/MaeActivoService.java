package com.sherwinca.apiacfsw.aplicacion;

import com.sherwinca.apiacfsw.infraestructura.mapper.MaeActivoMapper;
import com.sherwinca.apiacfsw.infraestructura.mapper.ParametroMapper;
import com.sherwinca.apiacfsw.infraestructura.repository.MaeactivoRepository;
import com.sherwinca.apiacfsw.modelo.acf.Maeactivo;
import com.sherwinca.apiacfsw.modelo.dto.MaeActivoDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import org.jboss.logging.Logger;

@ApplicationScoped
public class MaeActivoService {
    
    private static final Logger LOGGER = Logger.getLogger(MaeActivoService.class);

    @Inject
    MaeactivoRepository maeactivoRepository;

    @Inject
    MaeActivoMapper maeActivoMapper;
    
    @Inject
    ParametroMapper parametroMapper;

    public Long countActivo() throws Exception{
        try {
            
            return maeactivoRepository.countActivo();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeActivoService - [" + e.getMessage() + "]");
        }
    }

    public MaeActivoDTO buscarActivo(Long id_sistema) throws Exception {
        Maeactivo maf = maeactivoRepository.buscarActivo(id_sistema);
        return maf != null ? maeActivoMapper.vwMaeActivoToDTO(maf) : null;
    }

    public List<MaeActivoDTO> buscarTodos() throws Exception{
        try {
            return maeactivoRepository.buscarTodos()
                                    .stream()
                                    .map(maeActivoMapper::vwMaeActivoToDTO)
                                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeActivoService - [" + e.getMessage() + "]");
        }
    }

    @Transactional
    public MaeActivoDTO guardarActivo(MaeActivoDTO activoDTO) throws Exception{
        try {
            return maeactivoRepository.guardarActivo(activoDTO);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeActivoService - [" + e.getMessage() + "]");
        }
        
    }

    @Transactional
    public MaeActivoDTO actualizarActivo(Maeactivo activoDTO) throws Exception{
        try {
            return maeactivoRepository.actualizarActivo(activoDTO);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeActivoService - [" + e.getMessage() + "]");
        }
    }
    

    
}
