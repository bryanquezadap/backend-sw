package com.sherwinca.apiacfsw.aplicacion;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.sherwinca.apiacfsw.infraestructura.mapper.vwParametroMapper;
import com.sherwinca.apiacfsw.infraestructura.repository.Vw_parametrosRepository;
import com.sherwinca.apiacfsw.modelo.dto.VwParametroDTO;

import org.jboss.logging.Logger;


@ApplicationScoped
public class Vw_parametrosService {
    private static final Logger LOGGER = Logger.getLogger(Vw_parametrosService.class);

    @Inject
    Vw_parametrosRepository vw_parametrosRepository;

    @Inject
    vwParametroMapper VwparametroMapper;

    
    public List<VwParametroDTO> buscarvwParametros() throws Exception{

        try {
            return vw_parametrosRepository.buscarvwParametros()
                                          .stream()
                                          .map(VwparametroMapper::vwparametroDTO)
                                          .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Err");
        }
    }
}
