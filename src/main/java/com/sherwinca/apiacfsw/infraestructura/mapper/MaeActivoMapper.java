package com.sherwinca.apiacfsw.infraestructura.mapper;
import com.sherwinca.apiacfsw.modelo.acf.Maeactivo;
import com.sherwinca.apiacfsw.modelo.dto.MaeActivoDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel ="cdi", uses = ParametroMapper.class)
public interface MaeActivoMapper {
    
    MaeActivoDTO vwMaeActivoToDTO(Maeactivo maeactivo);

    MaeActivoDTO toMaeActivoDTO(Maeactivo maeactivo);

    Maeactivo toEntity(MaeActivoDTO activoDTO);


}
