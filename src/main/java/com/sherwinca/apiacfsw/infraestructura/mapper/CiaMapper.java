package com.sherwinca.apiacfsw.infraestructura.mapper;

import java.util.List;

import com.sherwinca.apiacfsw.modelo.acf.Cia;
import com.sherwinca.apiacfsw.modelo.dto.CiaDTO;

import org.mapstruct.Mapper;

@Mapper(componentModel="cdi")
public interface CiaMapper {
   CiaDTO toCiaDto(Cia cia); 
   List<CiaDTO> toLstCiaDto(List<Cia> listParametros);
}
