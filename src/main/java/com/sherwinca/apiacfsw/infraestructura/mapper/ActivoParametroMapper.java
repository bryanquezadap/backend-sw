package com.sherwinca.apiacfsw.infraestructura.mapper;

import java.util.List;

import com.sherwinca.apiacfsw.modelo.acf.VwActivoParametro;
import com.sherwinca.apiacfsw.modelo.dto.ActivoParametroDTO;

import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface ActivoParametroMapper {
    ActivoParametroDTO activoParametroDTO(VwActivoParametro vw_activo_parametro);
    List<ActivoParametroDTO> toListActivoParametroDTO(List<VwActivoParametro> lstVwActivoParametro);
    
}
