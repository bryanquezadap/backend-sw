package com.sherwinca.apiacfsw.infraestructura.mapper;

import java.util.List;

import com.sherwinca.apiacfsw.modelo.acf.Parametros;
import com.sherwinca.apiacfsw.modelo.dto.ParametroDTO;

import org.mapstruct.Mapper;

@Mapper(componentModel ="cdi")
public interface ParametroMapper {
    
    ParametroDTO toParametroDTO(Parametros parametros);
    List<ParametroDTO> toLstParametroDTO(List<Parametros> listParametros);
    
}
