package com.sherwinca.apiacfsw.infraestructura.mapper;

import java.util.List;

import com.sherwinca.apiacfsw.modelo.acf.AcfCimagenes;
import com.sherwinca.apiacfsw.modelo.dto.AcfCimagenesDTO;

import org.mapstruct.Mapper;

@Mapper(componentModel="cdi")
public interface AcfCimagenesMapper {
    List<AcfCimagenesDTO> toLstacfCimagenesDto(List<AcfCimagenes> acfCimagenes);
    AcfCimagenes toEntity(AcfCimagenesDTO acfCimagenes);
    AcfCimagenesDTO toacfCimagenesDTO(AcfCimagenes acfCimagenes);


    
}
