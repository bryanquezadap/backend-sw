package com.sherwinca.apiacfsw.infraestructura.mapper;

import java.util.List;

import com.sherwinca.apiacfsw.modelo.acf.VwParametros;
import com.sherwinca.apiacfsw.modelo.dto.VwParametroDTO;

import org.mapstruct.Mapper;


@Mapper(componentModel="cdi")
public interface vwParametroMapper {
    VwParametroDTO vwparametroDTO(VwParametros vw_parametros);
    List<VwParametroDTO> toListvwParametroDTO(List<VwParametros> lstVwParametros);
}
