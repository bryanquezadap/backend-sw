package com.sherwinca.apiacfsw.infraestructura.repository;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.sherwinca.apiacfsw.infraestructura.mapper.AcfCimagenesMapper;
import com.sherwinca.apiacfsw.modelo.acf.AcfCimagenes;
import com.sherwinca.apiacfsw.modelo.dto.AcfCimagenesDTO;

import org.jboss.logging.Logger;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Sort;

@ApplicationScoped
public class AcfCimagenesRepository implements PanacheRepository<AcfCimagenes> {
    private static final Logger LOGGER = Logger.getLogger(AcfCimagenesRepository.class);

    @Inject
    AcfCimagenesMapper AcfCimagenesMapper;

    public List<AcfCimagenes> listarAcfimagenes() throws Exception{

        try {
            return listAll();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en acfCimagenesRepository -[" + e.getMessage() + "]");
        }
    }

    public List<AcfCimagenes> listarImagenActivo(Long id_sistema) throws Exception{
        try{
            // Map params = new HashMap();
            // StringBuilder jpql = new StringBuilder();
            // jpql.append("1 = 1");

            // if(id_sistema != null){
            //     jpql.append(" and id_sistema = :id_sistema");
            //     params.put("id_sistema", id_sistema);
            // }

            // return list(jpql.toString(), Sort.ascending("id_imagenes"), params);
            return list("id_sistema", id_sistema);
        } catch(Exception e){
            LOGGER.error(e.getMessage());
            throw new Exception("Error en acfCimagenesRepository - [" + e.getMessage() + "]");
        }
    }


    public AcfCimagenesDTO guardarImagenes(AcfCimagenesDTO acfCimagenesDTO) throws Exception{
        try {
            AcfCimagenes imagenesEntity = AcfCimagenesMapper.toEntity(acfCimagenesDTO);
            persist(imagenesEntity);
            return AcfCimagenesMapper.toacfCimagenesDTO(imagenesEntity);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en acfCimagenesRepository - [" + e.getMessage() + "]");
        }
    }

}
