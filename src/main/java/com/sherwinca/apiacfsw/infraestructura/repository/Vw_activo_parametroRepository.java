package com.sherwinca.apiacfsw.infraestructura.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.sherwinca.apiacfsw.modelo.acf.VwActivoParametro;

import org.jboss.logging.Logger;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class Vw_activo_parametroRepository implements PanacheRepository<VwActivoParametro> {
    private static final Logger LOGGER = Logger.getLogger(Vw_activo_parametroRepository.class);

    public List<VwActivoParametro> buscarActivosParametro() throws Exception{
        try {
            return listAll();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en vw_activo_parametroRepository - [" + e.getMessage() + "]");
        }
    }

    public VwActivoParametro buscarActivoParametro(Long id_sistema) throws Exception{
        try {
            return findById(id_sistema);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en vw_activo_parametroRepository - [" + e.getMessage() + "]");
        }
    }
}
