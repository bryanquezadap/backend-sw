package com.sherwinca.apiacfsw.infraestructura.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.sherwinca.apiacfsw.modelo.acf.Cia;

import org.jboss.logging.Logger;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class CiaRepository implements PanacheRepository<Cia> {
    private static final Logger LOGGER = Logger.getLogger(CiaRepository.class);

    public List<Cia> listarCia() throws Exception{
        try {
            return listAll();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en CiaRepository - [" + e.getMessage() + "]");
        }
    }
}
