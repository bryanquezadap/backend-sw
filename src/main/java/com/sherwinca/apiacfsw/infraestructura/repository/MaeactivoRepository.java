package com.sherwinca.apiacfsw.infraestructura.repository;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.sherwinca.apiacfsw.infraestructura.mapper.MaeActivoMapper;
import com.sherwinca.apiacfsw.modelo.acf.Maeactivo;
import com.sherwinca.apiacfsw.modelo.dto.MaeActivoDTO;

import org.hibernate.service.spi.ServiceException;
import org.jboss.logging.Logger;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class MaeactivoRepository implements PanacheRepositoryBase<Maeactivo, Long>{

    @Inject
    MaeActivoMapper maeActivoMapper;

    private static final Logger LOGGER = Logger.getLogger(MaeactivoRepository.class);

    public Long countActivo() throws Exception{
        try {
            return count();
        } catch (Exception e) {

            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeactivoRepository - [" + e.getMessage() + "]");
        }
    }
    
    public Maeactivo buscarActivo(Long id_sistema) throws Exception{
        try {
            return findById(id_sistema);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeactivoRepository - [" + e.getMessage() + "]");
        }
    }

    public List<Maeactivo> buscarTodos() throws Exception{
        try {
            return listAll();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeactivoRepository - [" + e.getMessage() + "]");
        }
    }

    public MaeActivoDTO guardarActivo(MaeActivoDTO maeActivoDTO) throws Exception{
        try {
            Maeactivo activoEntity = maeActivoMapper.toEntity(maeActivoDTO);
            persist(activoEntity);
            return maeActivoMapper.toMaeActivoDTO(activoEntity);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeactivoRepository - [" + e.getMessage() + "]");
        }
    }

    public MaeActivoDTO actualizarActivo(Maeactivo maeActivoEntity) throws Exception {
        try {
            if(maeActivoEntity.getId_sistema() == null){
                throw new ServiceException("Activo no tiene un ID");
            }
            Optional<Maeactivo> optional = MaeactivoRepository.this.findByIdOptional(maeActivoEntity.getId_sistema());
            if(optional.isEmpty()) {
                throw new ServiceException(String.format("Ningún activo se encontró con id_sistema[%s]", maeActivoEntity.getId_sistema()));
            }

            Maeactivo activoEntity = optional.get();
            activoEntity.setCodigo(maeActivoEntity.getCodigo());
            activoEntity.setDescripcion(maeActivoEntity.getDescripcion());
            activoEntity.setAsignadoa(maeActivoEntity.getAsignadoa());
            activoEntity.setCliente_sucursal(maeActivoEntity.getCliente_sucursal());
            activoEntity.setCosto(maeActivoEntity.getCosto());
            activoEntity.setDepartamento(maeActivoEntity.getDepartamento());
            activoEntity.setDir_organizacional(maeActivoEntity.getDir_organizacional());
            activoEntity.setF_adicion(maeActivoEntity.getF_adicion());
            activoEntity.setF_adquisicion(maeActivoEntity.getF_adquisicion());
            activoEntity.setF_fin_depreciacion(maeActivoEntity.getF_fin_depreciacion());
            activoEntity.setF_ini_depreciacion(maeActivoEntity.getF_ini_depreciacion());
            activoEntity.setF_ult_inventario(maeActivoEntity.getF_ult_inventario());
            activoEntity.setF_ult_reparacion(maeActivoEntity.getF_ult_reparacion());
            activoEntity.setF_ult_revaluo(maeActivoEntity.getF_ult_revaluo());
            activoEntity.setF_ult_siniestro(maeActivoEntity.getF_ult_siniestro());
            activoEntity.setF_ult_siniestro(maeActivoEntity.getF_ult_siniestro());
            activoEntity.setG_f_fin(maeActivoEntity.getG_f_fin());
            activoEntity.setG_f_inicio(maeActivoEntity.getG_f_inicio());
            activoEntity.setGarantia(maeActivoEntity.getGarantia());
            activoEntity.setGerencia(maeActivoEntity.getGerencia());
            activoEntity.setInfo_adicional(maeActivoEntity.getInfo_adicional());
            activoEntity.setMarca(maeActivoEntity.getMarca());
            activoEntity.setModelo(maeActivoEntity.getModelo());
            activoEntity.setN_docrelacionado(maeActivoEntity.getN_docrelacionado());
            activoEntity.setPerdidaporuso(maeActivoEntity.getPerdidaporuso());
            activoEntity.setPerdidaporventa(maeActivoEntity.getPerdidaporventa());
            activoEntity.setPorcentaje(maeActivoEntity.getPorcentaje());
            activoEntity.setProveedor(maeActivoEntity.getProveedor());
            activoEntity.setResponsable(maeActivoEntity.getResponsable());
            activoEntity.setSerie(maeActivoEntity.getSerie());
            activoEntity.setUnidadnegocio(maeActivoEntity.getUnidadnegocio());
            activoEntity.setUtilidadporventa(maeActivoEntity.getUtilidadporventa());
            activoEntity.setValoradepreciar(maeActivoEntity.getValoradepreciar());
            activoEntity.setValordeventa(maeActivoEntity.getValordeventa());
            activoEntity.setValorenlibros(maeActivoEntity.getValorenlibros());
            activoEntity.setVidautil_anios(maeActivoEntity.getVidautil_anios());
            activoEntity.setVidautil_meses(maeActivoEntity.getVidautil_meses());
            activoEntity.setVresidual(maeActivoEntity.getVresidual());
            activoEntity.setParametros(maeActivoEntity.getParametros());
            MaeactivoRepository.this.persist(activoEntity);
            return maeActivoMapper.toMaeActivoDTO(activoEntity);

        } catch (Exception e) {

            LOGGER.error(e.getMessage());
            throw new Exception("Error en MaeactivoRepository - [" + e.getMessage() + "]");
        }
    }

}
