package com.sherwinca.apiacfsw.infraestructura.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.sherwinca.apiacfsw.modelo.acf.VwParametros;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import org.jboss.logging.Logger;

@ApplicationScoped
public class Vw_parametrosRepository implements PanacheRepository<VwParametros>{
    private static final Logger LOGGER = Logger.getLogger(Vw_parametrosRepository.class);

    public List<VwParametros> buscarvwParametros() throws Exception{
        try{
            return listAll();

        }catch (Exception e){
            LOGGER.error(e.getMessage());
            throw new Exception("Error en vw_parametrosRepository - ["+ e.getMessage() +"]");
        }
    }

    
}
