package com.sherwinca.apiacfsw.utils;

import java.util.Map;

public class MapUtils {
    public static boolean isNullOrEmpty(Map map, String key){
        return map.containsKey(key) && map.get(key) != null;
    }
}
