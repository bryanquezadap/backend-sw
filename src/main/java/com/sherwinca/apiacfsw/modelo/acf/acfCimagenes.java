package com.sherwinca.apiacfsw.modelo.acf;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;


@Entity
@Table(name="ACF_CIMAGENES")
public class AcfCimagenes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_imagenes;
    private Long id_sistema;
    private Date fecha;
    @Lob()
    private Byte[] imagen;
    private String nombre_archivo;
    private Long size_archivo;
    private String tipo_archivo;
    private String base64_archivo;

    public AcfCimagenes(){
        
    }
    public Long getId_imagenes() {
        return this.id_imagenes;
    }

    public void setId_imagenes(Long id_imagenes) {
        this.id_imagenes = id_imagenes;
    }

    public Long getId_sistema() {
        return this.id_sistema;
    }

    public void setId_sistema(Long id_sistema) {
        this.id_sistema = id_sistema;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Byte[] getImagen() {
        return this.imagen;
    }

    public void setImagen(Byte[]  imagen) {
        this.imagen = imagen;
    }

    public String getNombre_archivo() {
        return this.nombre_archivo;
    }

    public void setNombre_archivo(String nombre_archivo) {
        this.nombre_archivo = nombre_archivo;
    }

    public Long getSize_archivo() {
        return this.size_archivo;
    }

    public void setSize_archivo(Long size_archivo) {
        this.size_archivo = size_archivo;
    }

    public String getTipo_archivo() {
        return this.tipo_archivo;
    }

    public void setTipo_archivo(String tipo_archivo) {
        this.tipo_archivo = tipo_archivo;
    }

    public String getBase64_archivo() {
        return this.base64_archivo;
    }

    public void setBase64_archivo(String base64_archivo) {
        this.base64_archivo = base64_archivo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AcfCimagenes that = (AcfCimagenes) o;
        return id_imagenes.equals(that.id_imagenes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_imagenes);
    }


}