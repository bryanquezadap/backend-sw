package com.sherwinca.apiacfsw.modelo.acf;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="VW_PARAMETROS")
public class VwParametros {
    
    @Id
    private Long id;
    private Long id_parametro;
    private String nombre;
    private Long id_parametrodet;
    private String descripcion;

    public VwParametros(){}

    public Long getId_parametro() {
        return this.id_parametro;
    }

    public void setId_parametro(Long id_parametro) {
        this.id_parametro = id_parametro;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getId_parametrodet() {
        return this.id_parametrodet;
    }

    public void setId_parametrodet(Long id_parametrodet) {
        this.id_parametrodet = id_parametrodet;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
