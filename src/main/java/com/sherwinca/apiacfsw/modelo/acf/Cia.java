package com.sherwinca.apiacfsw.modelo.acf;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ACF_CIA")
public class Cia implements Serializable {
    @Id
    private Integer codcia;
    private String nomcom;
    private String razsoc;
    private String codpais;

    public Cia(){}


    public Integer getCodcia() {
        return this.codcia;
    }

    public void setCodcia(Integer codcia) {
        this.codcia = codcia;
    }

    public String getNomcom() {
        return this.nomcom;
    }

    public void setNomcom(String nomcom) {
        this.nomcom = nomcom;
    }

    public String getRazsoc() {
        return this.razsoc;
    }

    public void setRazsoc(String razsoc) {
        this.razsoc = razsoc;
    }

    public String getCodpais() {
        return this.codpais;
    }

    public void setCodpais(String codpais) {
        this.codpais = codpais;
    }

}
