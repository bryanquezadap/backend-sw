package com.sherwinca.apiacfsw.modelo.acf;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="ACF_PARAMETROSDET")
public class Parametros implements Serializable {
    @Id
    @Column(name = "ID_PARAMETRODET")
    public Long id_parametrodet;
    public String descripcion;

    // @ManyToMany(mappedBy = "parametros")
    // public List<Maeactivo> maeactivo;
    
    public Parametros () {}

    public Long getId_parametrodet() {
        return this.id_parametrodet;
    }

    public void setId_parametrodet(Long id_parametrodet) {
        this.id_parametrodet = id_parametrodet;
    }


    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Parametros(Long id_parametrodet, String descripcion )
    {
        this.id_parametrodet = id_parametrodet;
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parametros that = (Parametros) o;
        return id_parametrodet.equals(that.id_parametrodet) && descripcion.equals(that.descripcion);
    }

    @Override
    public int hashCode(){
        return Objects.hash(id_parametrodet, descripcion);
    }

    @Override
    public String toString(){
        return "Parametros{"+
            "id_parametrodet="+ id_parametrodet +
            ", descripcion='" + descripcion + '\'' +
            '}';
    }


}
