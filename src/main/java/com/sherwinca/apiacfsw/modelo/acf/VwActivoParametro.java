package com.sherwinca.apiacfsw.modelo.acf;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "VW_ACTIVOS_PARAMETROS")
public class VwActivoParametro {
    @Id
    private Long id_sistema;
    private String codigo;
    private String descripcion;
    private String marca;
    private String modelo;
    private String serie;
    private String info_adicional;
    private Float costo;
    private String asignadoa;
    private String dir_organizacional;
    private String gerencia;
    private String departamento;
    private String unidadnegocio;
    private String responsable;
    private Date f_adquisicion;
    private Date f_ini_depreciacion;
    private Date f_fin_depreciacion;
    private Date f_retiro;
    private Float vidautil_anios;
    private Integer vidautil_meses;
    private String porcentaje;
    private String proveedor;
    private String n_docrelacionado;
    private String garantia;
    private Date g_f_inicio;
    private Date g_f_fin;
    private Double vresidual;
    private Date f_ult_revaluo;
    private Date f_adicion;
    private Date f_ult_reparacion;
    private Date f_ult_siniestro;
    private Date f_ult_inventario;
    private String cliente_sucursal;
    private Double  valorenlibros;
    private Double  valoradepreciar;
    private Double  perdidaporuso;
    private Double  valordeventa;
    private Double  utilidadporventa;
    private Double  perdidaporventa;
    private String estados_activos;
    private String condicion_activo;
    private String en_calidad;
    private String tipo_activo;
    private String estado_disponible;
    private String clasificacion_activo;

    public Long getId_sistema() {
        return this.id_sistema;
    }

    public void setId_sistema(Long id_sistema) {
        this.id_sistema = id_sistema;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return this.modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getSerie() {
        return this.serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getInfo_adicional() {
        return this.info_adicional;
    }

    public void setInfo_adicional(String info_adicional) {
        this.info_adicional = info_adicional;
    }

    public Float getCosto() {
        return this.costo;
    }

    public void setCosto(Float costo) {
        this.costo = costo;
    }

    public String getAsignadoa() {
        return this.asignadoa;
    }

    public void setAsignadoa(String asignadoa) {
        this.asignadoa = asignadoa;
    }

    public String getDir_organizacional() {
        return this.dir_organizacional;
    }

    public void setDir_organizacional(String dir_organizacional) {
        this.dir_organizacional = dir_organizacional;
    }

    public String getGerencia() {
        return this.gerencia;
    }

    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    public String getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getUnidadnegocio() {
        return this.unidadnegocio;
    }

    public void setUnidadnegocio(String unidadnegocio) {
        this.unidadnegocio = unidadnegocio;
    }

    public String getResponsable() {
        return this.responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Date getF_adquisicion() {
        return this.f_adquisicion;
    }

    public void setF_adquisicion(Date f_adquisicion) {
        this.f_adquisicion = f_adquisicion;
    }

    public Date getF_ini_depreciacion() {
        return this.f_ini_depreciacion;
    }

    public void setF_ini_depreciacion(Date f_ini_depreciacion) {
        this.f_ini_depreciacion = f_ini_depreciacion;
    }

    public Date getF_fin_depreciacion() {
        return this.f_fin_depreciacion;
    }

    public void setF_fin_depreciacion(Date f_fin_depreciacion) {
        this.f_fin_depreciacion = f_fin_depreciacion;
    }

    public Date getF_retiro() {
        return this.f_retiro;
    }

    public void setF_retiro(Date f_retiro) {
        this.f_retiro = f_retiro;
    }

    public Float getVidautil_anios() {
        return this.vidautil_anios;
    }

    public void setVidautil_anios(Float vidautil_anios) {
        this.vidautil_anios = vidautil_anios;
    }

    public Integer getVidautil_meses() {
        return this.vidautil_meses;
    }

    public void setVidautil_meses(Integer vidautil_meses) {
        this.vidautil_meses = vidautil_meses;
    }

    public String getPorcentaje() {
        return this.porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getProveedor() {
        return this.proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getN_docrelacionado() {
        return this.n_docrelacionado;
    }

    public void setN_docrelacionado(String n_docrelacionado) {
        this.n_docrelacionado = n_docrelacionado;
    }

    public String getGarantia() {
        return this.garantia;
    }

    public void setGarantia(String garantia) {
        this.garantia = garantia;
    }

    public Date getG_f_inicio() {
        return this.g_f_inicio;
    }

    public void setG_f_inicio(Date g_f_inicio) {
        this.g_f_inicio = g_f_inicio;
    }

    public Date getG_f_fin() {
        return this.g_f_fin;
    }

    public void setG_f_fin(Date g_f_fin) {
        this.g_f_fin = g_f_fin;
    }

    public Double getVresidual() {
        return this.vresidual;
    }

    public void setVresidual(Double vresidual) {
        this.vresidual = vresidual;
    }

    public Date getF_ult_revaluo() {
        return this.f_ult_revaluo;
    }

    public void setF_ult_revaluo(Date f_ult_revaluo) {
        this.f_ult_revaluo = f_ult_revaluo;
    }

    public Date getF_adicion() {
        return this.f_adicion;
    }

    public void setF_adicion(Date f_adicion) {
        this.f_adicion = f_adicion;
    }

    public Date getF_ult_reparacion() {
        return this.f_ult_reparacion;
    }

    public void setF_ult_reparacion(Date f_ult_reparacion) {
        this.f_ult_reparacion = f_ult_reparacion;
    }

    public Date getF_ult_siniestro() {
        return this.f_ult_siniestro;
    }

    public void setF_ult_siniestro(Date f_ult_siniestro) {
        this.f_ult_siniestro = f_ult_siniestro;
    }

    public Date getF_ult_inventario() {
        return this.f_ult_inventario;
    }

    public void setF_ult_inventario(Date f_ult_inventario) {
        this.f_ult_inventario = f_ult_inventario;
    }

    public String getCliente_sucursal() {
        return this.cliente_sucursal;
    }

    public void setCliente_sucursal(String cliente_sucursal) {
        this.cliente_sucursal = cliente_sucursal;
    }

    public Double getValorenlibros() {
        return this.valorenlibros;
    }

    public void setValorenlibros(Double valorenlibros) {
        this.valorenlibros = valorenlibros;
    }

    public Double getValoradepreciar() {
        return this.valoradepreciar;
    }

    public void setValoradepreciar(Double valoradepreciar) {
        this.valoradepreciar = valoradepreciar;
    }

    public Double getPerdidaporuso() {
        return this.perdidaporuso;
    }

    public void setPerdidaporuso(Double perdidaporuso) {
        this.perdidaporuso = perdidaporuso;
    }

    public Double getValordeventa() {
        return this.valordeventa;
    }

    public void setValordeventa(Double valordeventa) {
        this.valordeventa = valordeventa;
    }

    public Double getUtilidadporventa() {
        return this.utilidadporventa;
    }

    public void setUtilidadporventa(Double utilidadporventa) {
        this.utilidadporventa = utilidadporventa;
    }

    public Double getPerdidaporventa() {
        return this.perdidaporventa;
    }

    public void setPerdidaporventa(Double perdidaporventa) {
        this.perdidaporventa = perdidaporventa;
    }

    public String getEstados_activos() {
        return this.estados_activos;
    }

    public void setEstados_activos(String estados_activos) {
        this.estados_activos = estados_activos;
    }

    public String getCondicion_activo() {
        return this.condicion_activo;
    }

    public void setCondicion_activo(String condicion_activo) {
        this.condicion_activo = condicion_activo;
    }

    public String getEn_calidad() {
        return this.en_calidad;
    }

    public void setEn_calidad(String en_calidad) {
        this.en_calidad = en_calidad;
    }

    public String getTipo_activo() {
        return this.tipo_activo;
    }

    public void setTipo_activo(String tipo_activo) {
        this.tipo_activo = tipo_activo;
    }

    public String getEstado_disponible() {
        return this.estado_disponible;
    }

    public void setEstado_disponible(String estado_disponible) {
        this.estado_disponible = estado_disponible;
    }

    public String getClasificacion_activo() {
        return this.clasificacion_activo;
    }

    public void setClasificacion_activo(String clasificacion_activo) {
        this.clasificacion_activo = clasificacion_activo;
    }
    
}
