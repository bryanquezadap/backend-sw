package com.sherwinca.apiacfsw.modelo.dto;

import java.sql.Date;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class AcfCimagenesDTO {
  
    public Long id_imagenes;
    public Long id_sistema;
    public Date fecha;
    public Byte[] imagen;
    public String nombre_archivo;
    public Long size_archivo;
    public String tipo_archivo;
    public String base64_archivo;

}
