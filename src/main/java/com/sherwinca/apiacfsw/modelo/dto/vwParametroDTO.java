package com.sherwinca.apiacfsw.modelo.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class VwParametroDTO {
    
    public Long id_parametro;
    public String nombre;
    public Long id_parametrodet;
    public String descripcion;

}
