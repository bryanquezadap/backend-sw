package com.sherwinca.apiacfsw.modelo.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class CiaDTO {
    public Integer codcia;
    public String nomcom;
    public String razsoc;
    public String codpais;
}
