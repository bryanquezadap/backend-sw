package com.sherwinca.apiacfsw.modelo.dto;

import java.util.Date;
import java.util.List;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class MaeActivoDTO {
    public Long id_sistema;

    public String codigo;
    public String descripcion;
    public String marca;
    public String modelo;
    public String serie;
    public String info_adicional;
    public Float costo;
    public String asignadoa;
    public String dir_organizacional;
    public String gerencia;
    public String departamento;
    public String unidadnegocio;
    public String responsable;
    public Date f_adquisicion;
    public Date f_ini_depreciacion;
    public Date f_fin_depreciacion;
    public Date f_retiro;
    public Float vidautil_anios;
    public Integer vidautil_meses;
    public String porcentaje;
    public String proveedor;
    public String n_docrelacionado;
    public String garantia;
    public Date g_f_inicio;
    public Date g_f_fin;
    public Double vresidual;
    public Date f_ult_revaluo;
    public Date f_adicion;
    public Date f_ult_reparacion;
    public Date f_ult_siniestro;
    public Date f_ult_inventario;
    public String cliente_sucursal;
    public Double  valorenlibros;
    public Double  valoradepreciar;
    public Double  perdidaporuso;
    public Double  valordeventa;
    public Double  utilidadporventa;
    public Double  perdidaporventa;
    public List<ParametroDTO> parametros;
}
