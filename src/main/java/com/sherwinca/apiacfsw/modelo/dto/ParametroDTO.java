package com.sherwinca.apiacfsw.modelo.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class ParametroDTO {

    public Long id_parametrodet;
    public String descripcion;
    
}
