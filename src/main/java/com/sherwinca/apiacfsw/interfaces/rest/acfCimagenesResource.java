package com.sherwinca.apiacfsw.interfaces.rest;

import java.math.BigInteger;
import java.util.List;
import javax.ws.rs.POST;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.validation.Valid;


import com.sherwinca.apiacfsw.aplicacion.AcfCimagenesService;
import com.sherwinca.apiacfsw.modelo.dto.AcfCimagenesDTO;

import org.jboss.logging.Logger;


@Path("/Imagenes")
public class AcfCimagenesResource {
    private static final Logger LOGGER = Logger.getLogger(AcfCimagenesResource.class);

    @Inject
    AcfCimagenesService AcfCimagenesService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarAcfCimagenes(){
        try {
            List<AcfCimagenesDTO> listimagenes = AcfCimagenesService.listarAcfimagenes();
            if(listimagenes == null || listimagenes.isEmpty()){
                return Response.status(Response.Status.NOT_FOUND).entity("No se encontraron Registros").build();
            }
            return Response.ok(listimagenes).build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrrio un problema ["+ e.getMessage() + "]").build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id_sistema}")
    public Response listarImagenActivo(@PathParam("id_sistema") Long id_sistema){
        try {
            List<AcfCimagenesDTO> listarImagenActivo = AcfCimagenesService.buscarImagenActivo(id_sistema);
            if(listarImagenActivo == null || listarImagenActivo.isEmpty()){
                return Response.status(Response.Status.NOT_FOUND).entity("No se encontraron registros").build();
            }

            return Response.ok(listarImagenActivo).build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrrio un problema ["+ e.getMessage() + "]").build();
        }
    }

    @POST
    public Response guardarActivo(@Valid AcfCimagenesDTO AcfCimagenesDTO) throws Exception{
        try {
            final AcfCimagenesDTO guardado = AcfCimagenesService.guardarImagenes(AcfCimagenesDTO);
            return Response.status(Response.Status.CREATED).entity(guardado).build();
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrio un problema [" + e.getMessage() + "]").build();
        }
    }
    
}
