package com.sherwinca.apiacfsw.interfaces.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sherwinca.apiacfsw.aplicacion.Vw_parametrosService;
import com.sherwinca.apiacfsw.modelo.dto.VwParametroDTO;

import org.jboss.logging.Logger;


@Path("/vwParametros")
public class Vw_ParametrosResource {
    private static final Logger LOGGER = Logger.getLogger(Vw_ParametrosResource.class);

    @Inject
    Vw_parametrosService vw_parametrosService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarvwParametros(){
        try {
            List<VwParametroDTO> lstvwParametro = vw_parametrosService.buscarvwParametros();

            if(lstvwParametro == null || lstvwParametro.isEmpty()){
                return Response.status(Response.Status.NOT_FOUND).entity("No se encontraron registros").build();
            }
            return Response.ok(lstvwParametro).build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrio un problema ["+ e.getMessage() + "]").build();
        }
    }
    
}
