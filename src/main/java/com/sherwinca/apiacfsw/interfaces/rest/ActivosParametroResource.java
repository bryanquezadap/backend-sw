package com.sherwinca.apiacfsw.interfaces.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sherwinca.apiacfsw.aplicacion.ActivoParametroService;
import com.sherwinca.apiacfsw.modelo.dto.ActivoParametroDTO;

import org.jboss.logging.Logger;

@Path("/vwMaf")
public class ActivosParametroResource {
    private static final Logger LOGGER = Logger.getLogger(ActivosParametroResource.class);

    @Inject
    ActivoParametroService activoParametroService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarActivosParametros(){
        try {
            List<ActivoParametroDTO> lstActivoParametros = activoParametroService.buscarActivoParametros();

            if(lstActivoParametros == null || lstActivoParametros.isEmpty()){
                return Response.status(Response.Status.NOT_FOUND).entity("No se encontraron registros").build();

            }

            return Response.ok(lstActivoParametros).build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrió un problema ["+ e.getMessage() + "]").build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id_sistema}")
    public Response buscarActivos(@PathParam("id_sistema") Long id_sistema){
        try {
            ActivoParametroDTO activos = activoParametroService.buscarActivoParametro(id_sistema);
            if (activos == null){
                return Response.status(Response.Status.NOT_FOUND).entity("No se encontraron registros").build();
            }
            return Response.ok(activos).build();
        } catch (Exception e) {
            return Response.serverError().entity("Ha ocurrido un problema").build();
        }
    }
}
