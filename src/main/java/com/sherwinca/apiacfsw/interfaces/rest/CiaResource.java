package com.sherwinca.apiacfsw.interfaces.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sherwinca.apiacfsw.aplicacion.CiaService;
import com.sherwinca.apiacfsw.modelo.dto.CiaDTO;

import org.jboss.logging.Logger;

@Path("/cia")
public class CiaResource {
    private static final Logger LOGGER = Logger.getLogger(CiaResource.class);

    @Inject
    CiaService ciaService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarCias(){
        try {
            List<CiaDTO> lstCia = ciaService.listarCia();
            if(lstCia == null || lstCia.isEmpty())
            {
                return Response.status(Response.Status.NOT_FOUND).entity("No se encontraron registros").build();
            }
            return Response.ok(lstCia).build();
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrió un problema ["+ e.getMessage() + "]").build();
        }
    }
}
