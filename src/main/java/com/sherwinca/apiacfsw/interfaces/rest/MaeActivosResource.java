package com.sherwinca.apiacfsw.interfaces.rest;


import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

import com.sherwinca.apiacfsw.aplicacion.MaeActivoService;
import com.sherwinca.apiacfsw.modelo.acf.Maeactivo;
import com.sherwinca.apiacfsw.modelo.dto.MaeActivoDTO;

import org.jboss.logging.Logger;

@Path("/maf")
public class MaeActivosResource {
    private static final Logger LOGGER = Logger.getLogger(MaeActivosResource.class);

    @Inject
    MaeActivoService maeActivoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarTodos(){
        try {
            List<MaeActivoDTO> lstActivos = maeActivoService.buscarTodos();
            if(lstActivos == null || lstActivos.isEmpty()){
                return Response.status(Response.Status.NOT_FOUND).entity("No se encontraron registros").build();
            }

            return Response.ok(lstActivos).build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrio un problema [ " + e.getMessage() + " ]").build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/count")
    public Response contarActivo(){
        try {
            Long contarActivo = maeActivoService.countActivo();
            return Response.ok(contarActivo).build();

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrio un problema [ " + e.getMessage() + " ]").build();
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id_sistema}")
    public Response buscarActivos(@PathParam("id_sistema") Long id_sistema){
        try {
            MaeActivoDTO activos = maeActivoService.buscarActivo(id_sistema);
            if (activos == null){
                return Response.status(Response.Status.NOT_FOUND).entity("No se encontraron registros").build();
            }
            return Response.ok(activos).build();
        } catch (Exception e) {
            return Response.serverError().entity("Ha ocurrido un problema").build();
        }
    }

    @POST
    public Response guardarActivo(@Valid MaeActivoDTO maeActivoDTO) throws Exception{
        try {
            final MaeActivoDTO guardado = maeActivoService.guardarActivo(maeActivoDTO);
            return Response.status(Response.Status.CREATED).entity(guardado).build();
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrio un problema [" + e.getMessage() + "]").build();
        }
    }

    @PUT
    public Response actualizarActivo(@Valid Maeactivo maeActivoDTO) throws Exception{
        try {
            final MaeActivoDTO guardado = maeActivoService.actualizarActivo(maeActivoDTO);
            return Response.ok(guardado).build();
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Response.serverError().entity("Ocurrio un problema [" + e.getMessage() + "]").build();
        }
    }
}
